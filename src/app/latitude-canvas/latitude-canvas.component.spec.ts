import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LatitudeCanvasComponent } from './latitude-canvas.component';

describe('LatitudeCanvasComponent', () => {
  let component: LatitudeCanvasComponent;
  let fixture: ComponentFixture<LatitudeCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LatitudeCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatitudeCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
