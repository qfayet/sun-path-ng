import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Point } from '@app/sun-path.service';

@Component({
  selector: 'app-latitude-canvas',
  templateUrl: './latitude-canvas.component.html',
  styleUrls: ['./latitude-canvas.component.css']
})
export class LatitudeCanvasComponent implements OnInit, AfterViewInit, OnChanges {

  @Input()
  size = 200;
  @Input()
  obliquity = 23;
  @Input()
  latitude = 45;

  @ViewChild('canvas')
  canvas: ElementRef;

  cx: CanvasRenderingContext2D;

  cos: number;
  sin: number;

  constructor() { }

  ngOnInit(): void {
  }

  rotate(center: number, x: number, y: number): Point {
    return new Point(
      (x - center) * this.cos + (y - center) * this.sin + center,
      - (x - center) * this.sin + (y - center) * this.cos + center,
    );
  }

  initTrigo() {
    let alpha = (this.obliquity / 360) * 2 * Math.PI;
    this.cos = Math.cos(alpha);
    this.sin = Math.sin(alpha);
  }

  updateView() {

    this.initTrigo();

    this.cx.clearRect(0, 0, this.size, this.size);

    let center = this.size / 2;
    let ray = this.size * 0.75 / 2;
    let margin = this.size * 0.075;

    let angle = (this.latitude / 360) * 2 * Math.PI;
    let rayCos = Math.max(Math.abs(ray * Math.cos(angle)), 5);
    let raySin = ray * Math.sin(angle);

    this.cx.strokeStyle = '#000';
    this.cx.beginPath();
    this.cx.arc(center, center, ray, 0, 2 * Math.PI);
    this.cx.stroke();

    let pnt: Point;

    this.cx.strokeStyle = '#000';
    this.cx.beginPath();
    pnt = this.rotate(center, center,  center - (ray + margin));
    this.cx.moveTo(pnt.x,  pnt.y);
    pnt = this.rotate(center, center, center + (ray + margin));
    this.cx.lineTo(pnt.x,  pnt.y);
    this.cx.stroke();

    this.cx.strokeStyle = '#00F';
    this.cx.beginPath();
    pnt = this.rotate(center, center - rayCos,  center - raySin);
    this.cx.moveTo(pnt.x,  pnt.y);
    pnt = this.rotate(center, center + rayCos,  center - raySin);
    this.cx.lineTo(pnt.x,  pnt.y);
    this.cx.stroke();

    this.cx.stroke();
  }

  ngAfterViewInit() {
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    canvas.width = this.size;
    canvas.height = this.size;
    this.cx = canvas.getContext('2d');
    this.updateView();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.cx) {
      this.updateView();
    }
  }

}
