import { Point, SunPathService, Trajectory } from './../sun-path.service';
import { Component, OnInit } from '@angular/core';
import { degToRad } from "../sun-path.service";
import { MatSliderChange } from '@angular/material/slider';


const DAY_MS = 1000 * 60 * 60 * 24;
const SPRING = 102;


function diffInDays(date1: Date, date2: Date): number {
  return Math.abs(date2.getTime() - date1.getTime()) / DAY_MS;
}


function dayOfYear(year: number, days: number): Date {
  let date = new Date(year, 0, 1);
  date.setDate(date.getDate() + days);
  return date;
}


@Component({
  selector: 'app-sun-path',
  templateUrl: './sun-path.component.html',
  styleUrls: ['./sun-path.component.css']
})
export class SunPathComponent implements OnInit {

  times = [...Array(96).keys()].map(i => i * 2 * Math.PI / 96);
  year: number;
  day_count: number;
  days: number;
  incli_deg = 23;
  lati_deg = 45;
  traj: Trajectory;
  pathPoints: Point[];
  chartPoints: Point[];
  sunPath: Point[];
  crop = true;

  constructor(
    public sunPathSvc: SunPathService,
  ) { }

  setTrajectory(traj: Trajectory) {
    this.traj = traj;
    this.pathPoints = traj.pathPoints(this.crop);
    this.chartPoints = traj.chartPoints();
  }

  updateTrajectory() {
    let date = ((this.days + SPRING)/ this.day_count) * 2.0 * Math.PI;
    let incli = degToRad(this.incli_deg);
    let lati = degToRad(90 - this.lati_deg);
    this.sunPathSvc.getTrajectory(this.times, incli, lati, date)
      .then(traj => this.setTrajectory(traj));
  }

  ngOnInit() {
    let now = new Date();
    this.year = now.getFullYear();
    let first = new Date(this.year, 0, 1);
    let last = new Date(this.year, 11, 31);
    this.day_count = diffInDays(first, last) + 1;
    this.days = diffInDays(first, now);
    this.updateTrajectory();
  }

  onObliquityChange(event: MatSliderChange) {
    this.incli_deg = event.value;
    this.updateTrajectory();
  }

  onLatitudeChange(event: MatSliderChange) {
    this.lati_deg = event.value;
    this.updateTrajectory();
  }

  onDateChange(event: MatSliderChange) {
    this.days = event.value;
    this.updateTrajectory();
  }

  displayLatitude(value: number): string {
    return `${value}°`
  }

  displayDate(value: number): string {
    let date = dayOfYear(this.year, this.days);
    let month = date.toLocaleString('en-us', { month: 'short' });
    return `${month} ${date.getDate()}`
  }

}
