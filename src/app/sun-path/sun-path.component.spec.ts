import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SunPathComponent } from './sun-path.component';

describe('SunPathComponent', () => {
  let component: SunPathComponent;
  let fixture: ComponentFixture<SunPathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SunPathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SunPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
