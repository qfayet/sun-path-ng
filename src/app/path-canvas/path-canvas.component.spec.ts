import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PathCanvasComponent } from './path-canvas.component';

describe('PathCanvasComponent', () => {
  let component: PathCanvasComponent;
  let fixture: ComponentFixture<PathCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PathCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PathCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
