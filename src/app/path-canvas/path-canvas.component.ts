import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Point } from '@app/sun-path.service'

@Component({
  selector: 'app-path-canvas',
  templateUrl: './path-canvas.component.html',
  styleUrls: ['./path-canvas.component.css']
})
export class PathCanvasComponent implements OnInit, AfterViewInit, OnChanges {

  @ViewChild('canvas')
  canvas: ElementRef;

  @Input()
  points: Point[];

  @Input()
  crop = false;
  @Input()
  size = 350;
  @Input()
  dash = 10;

  cx: CanvasRenderingContext2D;


  constructor() { }

  ngOnInit(): void {
  }

  toView(point: Point): Point {
    let delta = this.crop ? 90 : 180;
    let scale = this.size / (2 * delta);
    return new Point(
      (point.x + delta) * scale,
      this.size - (point.y + delta)  * scale,
    );
  }

  updateView() {

    this.cx.clearRect(0, 0, this.size, this.size);

    let center = this.size / 2;
    let ray = this.size / (this.crop ? 2 : 4);
    let dashDiag = this.dash / Math.sqrt(2);

    this.cx.strokeStyle = '#000';
    this.cx.beginPath();
    this.cx.arc(center, center, ray, 0, 2 * Math.PI);
    this.cx.stroke();

    this.cx.strokeStyle = '#000';
    this.cx.beginPath();
    this.cx.moveTo(center - this.dash, center);
    this.cx.lineTo(center + this.dash, center);
    this.cx.moveTo(center, center - this.dash);
    this.cx.lineTo(center, center + this.dash);
    this.cx.moveTo(center - dashDiag, center - dashDiag);
    this.cx.lineTo(center + dashDiag, center + dashDiag);
    this.cx.moveTo(center - dashDiag, center + dashDiag);
    this.cx.lineTo(center + dashDiag, center - dashDiag);
    this.cx.stroke();

    this.cx.strokeStyle = '#F00';
    this.cx.beginPath();
    let startx = 0;
    let starty = 0;
    this.points.forEach((point, i) => {
      // console.log(i, point);
      let viewPoint = this.toView(point);
      if (i == 0) {
        startx = viewPoint.x;
        starty = viewPoint.y;
        this.cx.moveTo(viewPoint.x, viewPoint.y);
      } else {
        this.cx.lineTo(viewPoint.x, viewPoint.y);
      }
    });
    if (!this.crop) {
      this.cx.lineTo(startx, starty); // from
    }

    this.cx.stroke();
  }

  ngAfterViewInit() {
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    canvas.width = this.size;
    canvas.height = this.size;
    this.cx = canvas.getContext('2d');
    this.updateView();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.cx) {
      this.updateView();
    }
  }

}
