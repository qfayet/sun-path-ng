import { Injectable } from '@angular/core';


export function radToDeg(rad: number) {
  return rad * 180 / Math.PI;
}

export function radToHour(rad: number) {
  return rad * 12 / Math.PI;
}

export function degToRad(deg: number) {
  return deg * Math.PI / 180;
}


export class Point {

  constructor(
    public x = 0,
    public y = 0,
    public z = 0,
  ) { }

}


export class SpherePoint {

  constructor(
    public alpha=0,
    public beta=0,
  ) { }

  xView() {
    return this.alpha * Math.cos(this.beta);
  }

  yView() {
    return - this.alpha * Math.sin(this.beta);
  }

  toView(): Point {
    return new Point(this.xView(), this.yView());
  }

  xViewDeg() {
    return radToDeg(this.xView());
  }

  yViewDeg() {
    return radToDeg(this.yView());
  }

  toViewDeg(): Point {
    return new Point(this.xViewDeg(), this.yViewDeg());
  }

  getHeight() {
    return Math.PI / 2 - this.alpha;
  }

  getHeightDeg() {
    return radToDeg(this.getHeight());
  }

  rotate(theta: number) {
    return new SpherePoint(this.alpha, this.beta + theta);
  }

  to3d() {
      let sina = Math.sin(this.alpha);
      let y = sina * Math.cos(this.beta);
      let z = sina * Math.sin(this.beta);
      return {x: Math.cos(this.alpha), y: y, z: z};
  }

  static from3d(x: number, y: number, z: number) {
    let alpha = Math.acos(x);
    let beta = Math.atan2(z, y);
    return new SpherePoint(alpha, beta);
  }

  reverse() {
    let {x, y, z} = this.to3d();
    return SpherePoint.from3d(y, x, z);
  }

  incline(delta: number) {
    return this.reverse().rotate(delta).reverse();
  }

  transform(theta: number, delta: number) {
    return this.rotate(theta).incline(delta);
  }

}


export const EQUATORIAL = new SpherePoint(Math.PI / 2);


export class Trajectory {

  public points: SpherePoint[];

  constructor(
    public times: number[],
    public point = EQUATORIAL,
    public delta = 0,
    public date = 0,
  ) {
    let theta = Math.PI / 2 - date;
    this.points = times.map(time => this.point.transform(theta + time, delta));
  }

  oblique(delta: number, date=0) {
    let point = this.point.transform(date, this.delta);
    return new Trajectory(this.times, point, delta, date);
  }

  pathPoints(crop = false): Point[] {
    let alpha_max = crop ? Math.PI / 2 + 0.01 : Math.PI - 1e-6;
    let points = this.points.filter(point => point.alpha < alpha_max);
    return points.map(point => point.toViewDeg());
  }

  chartPoints(): Point[] {
    return this.points.map((point, i) => 
      new Point(
        radToHour(this.times[i]),
        point.getHeightDeg(),
      ));
  }

}


@Injectable({
  providedIn: 'root'
})
export class SunPathService {

  constructor() { }

  async getTrajectory(times: number[], incli: number, lati: number, date: number) {
    let ecliptic = new Trajectory(times);
    let equator = ecliptic.oblique(incli);
    return equator.oblique(lati, date);
  }

}
