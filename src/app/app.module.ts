import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AppComponent } from './app.component';
import { PathPlotComponent } from './path-plot/path-plot.component';
import { SunPathComponent } from './sun-path/sun-path.component';
import { PathCanvasComponent } from './path-canvas/path-canvas.component';
import {MatSliderModule} from '@angular/material/slider';
import { LatitudeCanvasComponent } from './latitude-canvas/latitude-canvas.component';


@NgModule({
  declarations: [
    AppComponent,
    SunPathComponent,
    PathPlotComponent,
    PathCanvasComponent,
    LatitudeCanvasComponent
  ],
  imports: [
    BrowserModule, 
    FormsModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    MatSliderModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
