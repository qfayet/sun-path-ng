import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Point } from '@app/sun-path.service';

@Component({
  selector: 'app-path-plot',
  templateUrl: './path-plot.component.html',
  styleUrls: ['./path-plot.component.css']
})
export class PathPlotComponent implements OnInit, OnChanges {

  @Input()
  points: Point[];
  @Input()
  crop = false;

  multi: any[];
  view: any[] = [800, 500];

  // options
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Hours';
  yAxisLabel: string = 'Height';
  xAxisTicks = Array.from({length: 25}, (v, k) => k);
  yAxisTicks = Array.from({length: 13}, (v, k) => -90 + k * 15);
  timeline: boolean = false;
  xScaleMin = 0;
  xScaleMax = 24;
  yScaleMin = -90;
  yScaleMax = +90;

  constructor() {
  }

  ngOnInit(): void {
  }

  updateChart(): void {
    this.points.push(new Point(24, this.points[0].y));
    if (this.crop) {
      this.yScaleMin = 0;
      this.yAxisTicks = Array.from({length: 7}, (v, k) => k * 15);
      this.view[1] = 250;
      this.points = this.points.filter(pnt => pnt.y >= 0);
    }
    let seriePnts = this.points.map(pnt => {
      return {'name': pnt.x, 'value': pnt.y}
    });
    this.multi = [
      {
        "name": "Sun",
        "series": seriePnts,
      },
      {
        "name": "Axe",
        "series": [
          {'name': 0, 'value': 0},
          {'name': 24, 'value': 0}
        ],
      },
    ];
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateChart();
  }

  computeXTickFormatting(val: number) {
    return `${val}h`;
  }

  computeYTickFormatting(val: number) {
    return `${val}°`;
  }

  formatDeg(val: number) {
    return `${Math.round(val)}°`;
  }

  formatHour(val: number) {
    let hour = Math.floor(val);
    let min = Math.round((val - hour) * 60);
    if (min == 60) {
      hour += 1;
      min = 0;
    }
    let hourStr = hour < 10 ? `0${hour}` : `${hour}`;
    let minStr = min < 10 ? `0${min}` : `${min}`;
    return `${hourStr}H${minStr}`;
  }

}
