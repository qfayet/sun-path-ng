import { TestBed } from '@angular/core/testing';

import { SunPathService } from './sun-path.service';

describe('SunPathService', () => {
  let service: SunPathService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SunPathService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
